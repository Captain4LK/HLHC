/*
HLHC lang

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

//External includes
#include <stdio.h>
#include <string.h>
#include <ctype.h>
//-------------------------------------

//Internal includes
#include "HLHC.h"
//-------------------------------------

//#defines
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

int HC_scan_next(HC_State *s)
{
   int c;

   if(s->putback)
   {
      c = s->putback;
      s->putback = 0;

      return c;
   }

   c = fgetc(s->file);
   if(c=='\n')
      s->line++;

   return c;
}

int HC_scan_skip(HC_State *s)
{
   int c = HC_scan_next(s);
   while(strchr("\t\n\r\f ",c)!=NULL)
      c = HC_scan_next(s);

   return c;
}

int HC_scan(HC_State *s, HC_Token *t)
{
   int c = HC_scan_skip(s);

   switch(c)
   {
   case '+':
      t->token = HC_TOK_PLUS;
      break;
   case '-':
      t->token = HC_TOK_MINUS;
      break;
   case '*':
      t->token = HC_TOK_STAR;
      break;
   case '/':
      t->token = HC_TOK_SLASH;
      break;
   case '0': case '1': case '2': case '3': case '4':
   case '5': case '6': case '7': case '8': case '9':
      t->value_int = HC_scan_int(s,c);
      t->token = HC_TOK_INTLIT;
      break;
   case EOF:
      return 0;
   default:
      HC_log("HC_scan: unrecognised character (%c, line %d)\n",c,s->line);
      break;
   }

   return 1;
}

int HC_scan_int(HC_State *s, int c)
{
   int val = 0;

   while(isdigit(c))
   {
      val*=10;
      val+=(c-'0');
      c = HC_scan_next(s);
   }

   s->putback = c;
   return val;
}
//-------------------------------------
