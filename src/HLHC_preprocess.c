/*
HLHC lang

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

//External includes
#include <string.h>
//-------------------------------------

//Internal includes
#include "HLHC.h"
//-------------------------------------

//#defines
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

//Replaces trigraph sequences and splices lines
char *HC_preprocess_stage0(char *str)
{
   unsigned pos_slow;
   unsigned pos_fast;
   size_t length;

   //Pass 0 --> trigraphs
   pos_slow = 0;
   pos_fast = 0;
   length = strlen(str);
   while(str[pos_fast])
   {
      char replace = 0;
      if(str[pos_fast]=='?'&&(length-pos_fast)>=3&&str[pos_fast+1]=='?')
      {
         switch(str[pos_fast+2])
         {
         case '=': replace = '#'; break;
         case '/': replace = '\\'; break;
         case '\'': replace = '^'; break;
         case '(': replace = '['; break;
         case ')': replace = ']'; break;
         case '!': replace = '|'; break;
         case '<': replace = '{'; break;
         case '>': replace = '}'; break;
         case '-': replace = '~'; break;
         }
      }

      if(replace!=0)
      {
         str[pos_slow] = replace;
         pos_fast+=3;
         pos_slow++;
      }
      else
      {
         str[pos_slow] = str[pos_fast];
         pos_fast++;
         pos_slow++;
      }
   }
   str[pos_slow] = '\0';

   //Pass 1 --> line splicing
   pos_slow = 0;
   pos_fast = 0;
   length = strlen(str);
   while(str[pos_fast])
   {
      if(str[pos_fast]=='\\'&&(length-pos_fast)>=2&&str[pos_fast+1]=='\n')
      {
         pos_fast+=2;
      }
      else
      {
         str[pos_slow] = str[pos_fast];
         pos_fast++;
         pos_slow++;
      }
   }
   str[pos_slow] = '\0';

   return str;
}
//-------------------------------------
