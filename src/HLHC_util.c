/*
HLHC lang

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

//External includes
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
//-------------------------------------

//Internal includes
#include "HLHC.h"
//-------------------------------------

//#defines
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

char *HC_file_read(const char *path)
{
   FILE *f = fopen(path,"rb");
   if(f==NULL)
   {
      HC_log("HC_file_read: failed to open \'%s\'\n",path);
      return NULL;
   }

   char *buffer = NULL;
   size_t size = 0;
   fseek(f,0,SEEK_END);
   size = ftell(f);
   fseek(f,0,SEEK_SET);

   buffer = malloc(size+1);

   fread(buffer,size,1,f);
   buffer[size] = 0;

   fclose(f);

   return buffer;
}

void HC_log(const char *w, ...)
{
   va_list args;
   va_start(args,w);
   vprintf(w,args);
   va_end(args);
}
//-------------------------------------
