/*
HLHC lang

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

//External includes
#include <stdio.h>
#include <stdlib.h>
//-------------------------------------

//Internal includes
#include "HLHC.h"
//-------------------------------------

//#defines
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

HC_AST_node *HC_ast_node_new(HC_AST_node *left, HC_AST_node *right, HC_AST opc, int value_int)
{
   HC_AST_node *n = malloc(sizeof(*n));
   n->left = left;
   n->right = right;
   n->opc = opc;
   n->value_int = value_int;
   return n;
}

HC_AST_node *HC_ast_primary()
{
   HC_AST_node *n = NULL;

}

HC_AST HC_ast_tok2ast(HC_Tok tok)
{
   switch(tok)
   {
   case HC_TOK_PLUS: return HC_AST_ADD;
   case HC_TOK_MINUS: return HC_AST_SUB;
   case HC_TOK_STAR: return HC_AST_MUL;
   case HC_TOK_SLASH: return HC_AST_DIV;
   default: HC_log("Invalid token\n"); return HC_AST_ERROR;
   }
}
//-------------------------------------
