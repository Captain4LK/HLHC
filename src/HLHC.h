/*
HLHC lang

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

#ifndef _HLHC_H_

#define _HLHC_H_

//types

typedef enum
{
   HC_TOK_PLUS, HC_TOK_MINUS, HC_TOK_STAR, HC_TOK_SLASH, HC_TOK_INTLIT,
}HC_Tok;

typedef enum
{
   HC_AST_ADD, HC_AST_SUB, HC_AST_MUL, HC_AST_DIV, HC_AST_ERROR,
}HC_AST;

typedef struct
{
   HC_Tok token;
   int value_int;
}HC_Token;

typedef struct HC_AST_node
{
   HC_AST opc;
   struct HC_AST_node *left;
   struct HC_AST_node *right;
   int value_int;
}HC_AST_node;

typedef struct
{
   int putback;
   int line;
   FILE *file;
}HC_State;
//-------------------------------------

//functions
char *HC_preprocess_stage0(char *str);

int HC_scan_next(HC_State *s);
int HC_scan_skip(HC_State *s);
int HC_scan(HC_State *s, HC_Token *t);
int HC_scan_int(HC_State *s, int c);

HC_AST_node *HC_ast_node_new(HC_AST_node *left, HC_AST_node *right, HC_AST opc, int value_int);
HC_AST_node *HC_ast_primary();
HC_AST HC_ast_tok2ast(HC_Tok tok);

char *HC_file_read(const char *path);
void HC_log(const char *w, ...);
#define HC_log_line(w,...) do { char HC_log_line_tmp[512]; snprintf(HC_log_line_tmp,512,__VA_ARGS__); HC_log(w "(%s:%u): %s",__FILE__,__LINE__,HC_log_line_tmp); } while(0)
//-------------------------------------

#endif
